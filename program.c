#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <getopt.h>
#include <ctype.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <iomanip>
#include <errno.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <time.h>
#include <sys/stat.h>
#include <errno.h>
#include <vector>
using namespace std;

// sprawdzic czy nie usuwa plików młodszych z katalogu, przestawic date
void usun1(char *f, char *data)
{
	struct dirent *next_file;
	DIR *theFolder;
	char filepath[256];
	char tab[256];
	theFolder = opendir(f);
	
	int day1,month1,year1;
    int day2,month2,year2;
	
	struct stat attrib;			// create a file attribute structure
	
	while (next_file = readdir(theFolder))
	{
		sprintf(filepath, "%s/%s", f, next_file->d_name);
		
		stat(filepath, &attrib);		// get the attributes of afile.txt
		char date[10];
		strftime(date, 10, "%d-%m-%y", gmtime(&(attrib.st_ctime)));
		sscanf(date,"%d-%d-%d",&day1,&month1,&year1); //reads the numbers
		sscanf(data,"%d-%d-%d",&day2,&month2,&year2); //from the string
		//cout<<year1<<" "<<month1<<" "<<day1<<" "<<endl;
		if (year1<year2 || month1<month2 || day1<day2) //compares 2 dates
		{
			remove(filepath);
			
			if(opendir(filepath))
			{
				sprintf(tab, "exec rm -r %s", filepath);
				system(tab);
			}
			date[0] = 0;
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////
typedef vector<string> fileVect;

fdsfdsfsgfggfg
fileVect getFiles(string directory, char *data){
	
	char *aaa;
	int day1,month1,year1;
	int day2,month2,year2;
	char date[10];
	char tab[256];
	
    if(*(directory.end() - 1) != '/'){
        directory += "/";
    }
 
    DIR *dir = opendir(directory.c_str());
    fileVect tmp;
	
	struct stat attrib;
	
    if(dir){
        while(dirent *entry = readdir(dir)){
            if(strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")){
                string fileaddr = directory + entry->d_name;
                tmp.push_back(fileaddr);
				
				//cout<<fileaddr<<endl;
				aaa = new char [fileaddr.length() + 1];
				strcpy(aaa, fileaddr.c_str());
				stat(aaa, &attrib);
				
				strftime(date, 10, "%d-%m-%y", gmtime(&(attrib.st_ctime)));
				
				sscanf(date,"%d-%d-%d",&day1,&month1,&year1); //reads the numbers
				sscanf(data,"%d-%d-%d",&day2,&month2,&year2);
				
				if (year1<year2 || month1<month2 || day1<day2) //compares 2 dates
				{
					remove(aaa);
					
					if(opendir(aaa))
					{
						sprintf(tab, "exec rm -r %s", aaa);
						system(tab);
						//cout<<"Usunięto plik: "<<aaa<<endl;
						
					}
				}
			
                if(DIR *dir2 = opendir(fileaddr.c_str()))
				{
                    closedir(dir2);
                    fileVect tmp2 = getFiles(fileaddr, data);
                    tmp.insert(tmp.end(), tmp2.begin(), tmp2.end());
                }  
                date[0] = 0;
            }
           
        }
 
        closedir(dir);
    }
   
    return tmp;
}
 // fsdfidsgi
zmiana w programie 

int main(int argc,char** argv)
{	
		int i = 0;
		char *f;
		char *data;
		string rem;
		unsigned char c;
		int option_index = 0;
		char tab[256]; 
		int Abort = 0;
		int p = 0;
		
		if(argc == 1)
		{
			p = 2;
			fprintf(stderr,"\n************************************************************\nProgram usuwa pliki starsze od wprowadzonej daty.\nWymagane jest wprowadzenie sciezki i daty.\nDomyslnie katalogi przegladane sa od liscia do korzenia.\nPomoc:\n -d: sciezka katalogu\n -t: data - date nalezy wprowadzic w formacie: DD-MM-YY\n -l: przegląda pliki zaczynając od korzenia \n -h lub -- help: pomoc\n\nPrzykład uzycia: ./deleter -d /home/oldfiles/ -t 17-10-14\n************************************************************\n\n");
		}
		
		while(i <= argc)
		{
			static struct option long_options[] = {
			{"help", no_argument, 0, 'h' },
			{0, 0, 0,  0 }
			};

			if (c == -1)
				break;

			c = getopt_long(argc, argv, "d:k:n:h:t:l", long_options, &option_index);
			
			switch(c)
			{
				case 'd':
					struct stat sb;
					f = realpath(optarg, NULL);
					if (stat(f, &sb) == 0 && S_ISDIR(sb.st_mode))
					{
					cout<<"Wprowadzona scieżka: "<<f<<endl;
					
					

					
						Abort = 2;
						p++;
					}
					else
					{
						cout<<"Wprowadzona sciezka nie istnieje."<<endl;
						p = 2;
					}
				
					break;
					
				case 't':
					data = optarg;
					cout<<"Wprowadzona data: "<<data<<endl;
					p++;
					break;
					
				case 'l':
					Abort = 1;
					break;
					
				case 'h':
					fprintf(stderr,"\n************************************************************\nProgram usuwa pliki starsze od wprowadzonej daty.\nWymagane jest wprowadzenie sciezki i daty.\nDomyslnie katalogi przegladane sa od liscia do korzenia.\nPomoc:\n -d: sciezka katalogu\n -t: data - date nalezy wprowadzic w formacie: DD-MM-YY\n -l: przegląda pliki zaczynając od korzenia \n -h lub -- help: pomoc\n\nPrzykład uzycia: ./deleter -d /home/oldfiles/ -t 17-10-14\n************************************************************\n\n");
					p =2;
					break;
					
				case '?':
					break;
					
				case ':':
					cout<<"Prosze wprowadzic argument\n";    
					break;
			}	
			i++;
		}
		
		if( p >=2){
		if(Abort == 1)
		{
			usun1(f, data);
		}
		else if(Abort == 2)
		{
			string str(f, f + 1);
			fileVect files = getFiles(f, data);
		}
		}else{
			fprintf(stderr, "Prosze wprowadzic sciezke i date.\nPrzyklad:\n ./deleter -d /home/oldfiles/ -t 17-10-14\n");
		}
			
		return EXIT_SUCCESS;
}